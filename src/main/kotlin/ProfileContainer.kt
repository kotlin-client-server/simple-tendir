import kotlinx.browser.document
import kotlinx.html.*
import org.w3c.dom.HTMLElement
import org.w3c.dom.HTMLHeadingElement

class ProfileContainer {

    private val h1TextElement by lazy {
        document.getElementById("action_h1_element_text") as HTMLHeadingElement
    }

    fun TagConsumer<HTMLElement>.render() {
        div(classes = "profile_container") {
            div(classes = "logo_profile_container") {
                img(src = "svg/logo.svg")
            }
            div(classes = "profile_container_profile") {
                div(classes = "profile_photo") {
                    img(src = "https://vokrug-tv.ru/pic/product/1/5/3/5/153546b44b95acb2f01d82ff7f24a58d.jpg")
                }
                div(classes = "profile_action_buttons") {

                    val callback = { m: String -> h1TextElement.textContent = m }

                    ActionButton("dislike", "I DISLIKE this guy!", callback).run {
                        render()
                    }
                    ActionButton("superlike", "I SUPER LIKE GUY!", callback).run {
                        render()
                    }
                    ActionButton("like", "I LIKE this Nicy Guy!", callback).run {
                        render()
                    }
                }
            }

            div(classes = "profile_action_box") {
                h1 {
                    id = "action_h1_element_text"
                    +"ACTION: PUSH THE ACTION BUTTON  "
                }
            }
        }

    }
}