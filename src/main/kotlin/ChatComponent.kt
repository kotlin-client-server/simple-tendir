import kotlinx.browser.window
import kotlinx.html.*
import kotlinx.html.js.onMouseOutFunction
import kotlinx.html.js.onMouseOverFunction
import org.w3c.dom.HTMLElement
import org.w3c.dom.events.Event

private var counter = 0

class ChatComponent(private val name: String, private val lastMessage: String, private val photoUrl: String, private val flags: Collection<String>) {

    private val mouseOut: (Event) -> Unit = {
        rootDiv.asDynamic().style.backgroundColor = "inherit"
    }

    private val mouseOver: (Event) -> Unit = {
        rootDiv.asDynamic().style.backgroundColor = "lightblue"
    }

    private val chatComponentId = "chat_component_box${counter++}"
    private val rootDiv by lazy { window.document.getElementById(chatComponentId) }

    fun TagConsumer<HTMLElement>.render() {
        div("chat_component_box") {
            id = chatComponentId

            div(classes = "chat_component_photo_box") {
                img(src = photoUrl)
            }
            div(classes = "chat_component_text_box") {
                div(classes = "chat_component_header") {
                    h3(classes = "chat_component_name arial_helvetica_font") {
                        +name
                    }
                    div(classes = "chat_component_flags_box") {
//                        +"Match flags: ${flags.joinToString()}"
                    }
                }
                div(classes = "chat_component_message_preview arial_helvetica_font") {
                    +lastMessage
                }
            }

            onMouseOutFunction = mouseOut
            onMouseOverFunction = mouseOver

        }
    }
}