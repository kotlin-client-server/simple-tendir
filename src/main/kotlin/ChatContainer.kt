import kotlinx.html.*
import org.w3c.dom.HTMLElement

class ChatContainer {
    fun TagConsumer<HTMLElement>.render() {
        div(classes = "chat_container") {
            div(classes = "chat_container_my_profile") {
                div(classes = "chat_container_my_profile_box") {
                    div(classes = "chat_container_my_profile_box_photo") {
                        img(src = "https://www.clipartsfree.net/svg/74221-male-avatar-vector.svg")
                    }
                    div(classes = "chat_container_my_profile_box_caption arial_helvetica_font") {
                        +"My Profile"
                    }
                }
                div(classes = "chat_container_lookup_button_box") {
                    img(src = "svg/lookup.svg")
                }
            }
            div(classes = "chat_container_matches") {
                div(classes = "chat_container_matches_logo") {
                    img(src = "svg/matches.svg")
                }
                div(classes = "chat_container_matches_captions") {
                    div {
                        h3(classes = "arial_helvetica_font") {
                            +"Discover New Matches"
                        }
                    }
                    div(classes = "arial_helvetica_font") {
                        +"Start swiping to connect with new people!"
                    }
                }
            }
            div(classes = "chat_container_chats_container") {
                h3(classes = "chat_container_chats_caption arial_helvetica_font") {
                    +"Messages"
                }
                div(classes = "chat_container_chats_box") {
                    ChatComponent("Cat", "Mew Mew mew", "http://placeimg.com/80/80/animals", emptyList()).run {
                        render()
                    }
                    ChatComponent("Mice", "Pee Pee pee", "http://placeimg.com/80/70/animals", emptyList()).run {
                        render()
                    }
                    ChatComponent("Dog", "Gav Gav Gav", "http://placeimg.com/80/60/animals", emptyList()).run {
                        render()
                    }
                    ChatComponent("Sheep", "Bew Bew Bew", "http://placeimg.com/80/50/animals", emptyList()).run {
                        render()
                    }
                    ChatComponent("Cow", "Moo Moo moo", "http://placeimg.com/80/40/animals", emptyList()).run {
                        render()
                    }
                }
            }
        }
    }
}